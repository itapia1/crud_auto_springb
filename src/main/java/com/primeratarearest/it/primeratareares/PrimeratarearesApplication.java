package com.primeratarearest.it.primeratareares;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrimeratarearesApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrimeratarearesApplication.class, args);
	}

}
