package com.primeratarearest.it.primeratareares.entidad;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "auto")
@NoArgsConstructor
@AllArgsConstructor
public class Auto {
    @Id
    private int id_auto;
    private String marca;
    private int anio;
    private int cantidad_personas;
    private double precio_auto;

}
