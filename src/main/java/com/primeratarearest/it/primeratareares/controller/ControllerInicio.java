package com.primeratarearest.it.primeratareares.controller;

import com.primeratarearest.it.primeratareares.entidad.Auto;
import com.primeratarearest.it.primeratareares.services.AutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ControllerInicio {

    @Autowired
    AutoService autoServicio;
    @GetMapping("/all-cars")
    public List<Auto> getAllCars() {
      return  autoServicio.listadoAuto();
    }

    @GetMapping("/car")
    public Optional<Auto> getCar(@RequestParam int id_auto) {
        return  autoServicio.listarAutoById(id_auto);
    }

    @PostMapping("/create-car")
    public void createCar(@RequestBody Auto auto) {
        autoServicio.guardarAuto(auto);
    }

    @DeleteMapping("/delete-car")
    public void deleteCar(@RequestParam  int id_auto) {
        autoServicio.eliminarAuto(id_auto);
    }

    @PutMapping("/update-car")
    public void updateCar(@RequestBody  Auto auto) {
        autoServicio.updateAuto(auto);
    }

}

