package com.primeratarearest.it.primeratareares.services;

import com.primeratarearest.it.primeratareares.entidad.Auto;
import com.primeratarearest.it.primeratareares.repositorio.AutoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AutoService {

    @Autowired
    AutoRepositorio autoRepositorio;

    public List<Auto> listadoAuto() {
       return autoRepositorio.findAll();
    }

    public void guardarAuto(Auto auto) {
        if (!autoRepositorio.existsById(auto.getId_auto()))
            autoRepositorio.save(auto);
        else{
            System.out.println("Paisano ese carro ya existe compa, no se puede crear como nuevo");
        }
    }

    public void eliminarAuto(int id_auto) {
        autoRepositorio.deleteById(id_auto);
    }
    public void updateAuto(Auto auto){
        if (autoRepositorio.existsById(auto.getId_auto()))
                autoRepositorio.save(auto);
        else{
            System.out.println("Hey vale mia ese carro no existe compa");
        }
    }
    public Optional<Auto> listarAutoById(int idAuto) {
       return autoRepositorio.findById(idAuto);
    }
}
