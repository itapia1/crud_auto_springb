package com.primeratarearest.it.primeratareares.repositorio;

import com.primeratarearest.it.primeratareares.entidad.Auto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AutoRepositorio extends JpaRepository<Auto, Integer> {

}
